from sympy import isprime as test_premier
from random import randint
from tkinter import *

class jeu:
    def init():
        print('Vous avez commencé une partie de Juniper Green')
        nbrMax = int(input('Quel est le nombre maximal de cette partie ? '))
        test = False
        while test==False:
            nbrJoueurs = int(input('Combien de joueurs vont jouer ? '))
            if nbrJoueurs<=4:
                test = True
            else:
                print('Il y a trop de joueurs, choisissez un autre nombre.')
        rep=input('Voulez-vous choisir un nom ? ')
        joueur=[]       #liste des noms des joueurs
        if rep=='oui':
            for i in range(nbrJoueurs):
                print('Joueur numéro', i+1, 'rentrez votre nom : ')
                nom=input()
                joueur.append(nom)
        else:
            for i in range(nbrJoueurs):
                joueur.append('j'+str(i+1))
        jeu.debut_du_jeu(nbrMax, joueur)

    def debut_du_jeu(nbrMax, joueur):
        print('Choisissez le premier nombre,', joueur[0])
        print('Il ne doit pas être un nombre premier.')
        test = True
        while test==True:
            val = int(input())
            test = test_premier(val)
            if test==True:
                print('Le nombre choisi est invalide, choisissez un autre nombre.')
        nbrDonnes=[val]  #aucun nombre n'a encore été donné
        vue.afficher(nbrDonnes)
        if len(joueur)==1:  # s'il n'y a qu'un seul joueur, on lance une partie contre l'ordinateur
            nom = joueur[0]
            jeu.jeu_un_joueur(nbrMax, nom, val, nbrDonnes)
        else:
            jeu.jeu(nbrMax, joueur, val, nbrDonnes, 1)


    def jeu(nbrMax, nom, valPrecedent, nbrDonnes, joueur_actuel):
        test=False
        for i in range(3):      # on demande au joueur actuel un nombre
            if test==False:     # s'il donne trois nombres faux, il perd
                print(nom[joueur_actuel], ', quel nombre voulez vous choisir ? ')
                val = int(input())
                if vue.verifier(val, valPrecedent, nbrMax, nbrDonnes):
                    test=True
                    nbrDonnes.append(val)
                    vue.afficher(nbrDonnes)
                else:
                    print('La valeur est erronnée.')
                    if i==2:
                        print(nom[joueur_actuel], 'a perdu')
                        rep=input('Voulez-vous faire une autre partie ? ')
                        if rep=='oui':
                            return vue.reinit(nom)
                        return
                    print('Il reste', 2-i, 'chances.')
        if joueur_actuel==len(nom)-1:   # change le joueur actuel
            joueur_actuel=0
        else:
            joueur_actuel+=1
        jeu.jeu(nbrMax, nom, val, nbrDonnes, joueur_actuel)


    def jeu_un_joueur(nbrMax, nom, valPrecedent, nbrDonnes):
        rep_ordi = jeu.tour_ordi(nbrMax, valPrecedent, nbrDonnes) # nombre donné par l'ordinateur
        if rep_ordi==0:
            print("Bravo ! Vous avez gagné !")
            rep=input('Voulez-vous faire une autre partie ? ')
            if rep=='oui':
                return vue.reinit(nom)
            return
        nbrDonnes.append(rep_ordi)
        print("L'ordinateur a joué le nombre ", rep_ordi)
        test=False
        for i in range(3):
            if test==False:
                print(nom, ', quel nombre voulez vous choisir ? ')
                vue.afficher(nbrDonnes)
                val = int(input())
                if vue.verifier(val, rep_ordi, nbrMax, nbrDonnes):
                    test=True
                    nbrDonnes.append(val)
                else:
                    print('La valeur est erronnée.')
                    vue.afficher(nbrDonnes)
                    if i==2:
                        print('Vous avez perdu')
                        rep=input('Voulez-vous faire une autre partie ? ')
                        if rep=='oui':
                            return vue.reinit(nom)
                        return
                    print('Il reste', 2-i, 'chances.')
        jeu.jeu_un_joueur(nbrMax, nom, val, nbrDonnes)

    def tour_ordi(nbrMax, valPrecedent, nbrDonnes):
        if valPrecedent==1:
            for i in range(nbrMax):
                if test_premier(nbrMax-i) and nbrMax-i not in nbrDonnes:
                    return nbrMax-i
        list=[]         # utilise la base de mult et div pour trouver les nombres possibles
        v=1
        while v<valPrecedent:
            if valPrecedent%v==0 and v not in nbrDonnes:
                list.append(v)
            v+=1
        while v<=nbrMax:
            v+=valPrecedent
            if v not in nbrDonnes and v<=nbrMax:
                list.append(v)
        if len(list)==0:
            return 0
        else:
            if 1 in list and len(list)>=2:     # renvoie un nombre aléatoire parmi ceux possibles
                list.remove(1)                  # si possible, ne renvoie pas 1 qui mènerais à une défaite directe
            return list[randint(0, len(list)-1)]

class vue:
    def reinit(nom):        # permet de relancer une partie avec les même joueurs que précédemment
        nbrMax = int(input('Quel est le nombre maximal de cette nouvelle partie ? '))
        jeu.debut_du_jeu(nbrMax, nom)

    def verifier(val, valPrecedent, nbrMax, nbrDonnes):
        if val>nbrMax:      # si le nombre est trop gros, on le refuse
            return False
        if val in nbrDonnes:    # si le nombre est déjà donné, on le refuse
            return False
        if val in vue.div(valPrecedent) or val in vue.mult(valPrecedent, nbrMax):
            return True   # si le nombre est un diviseur ou un multiple, on l'accepte
        else :      # sinon, on le refuse
            return False

    def div(val):      # renvoie la liste des diviseurs de val
        list=[]
        v=1
        while v<val:
            if val%v==0:
                list.append(v)
            v+=1
        return list

    def mult(val, nbrMax):      # renvoie la liste des multiples de val inférieurs à nbrMax
        list=[]
        v=val
        while v<nbrMax:
            v+=val
            list.append(v)
        return list

    def afficher(nbrDonnes):
        print("Voici les nombres joués", nbrDonnes)

jeu.init()
